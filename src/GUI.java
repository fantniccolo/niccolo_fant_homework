import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

public class GUI {
	String indirizzo[] = {"informatica", "elettronica","elettrotecnica","occhiale","energia","meccanica"};
	String sezione[] = {"A", "B","C","D","E","F"};
	String classe[] = {"1", "2","3","4","5"};
	String nazione[]=new String[1000];
	String regione[]= new String[1000];
	String comune[] = new String[10000];
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String SCUOLA = "jdbc:mysql://localhost/scuola";
	static final String NAZIONE ="jdbc:mysql://localhost/nazionI";
	
	static final String USER = "root";
	static final String PASS = "ciao";
	private JTextField nomeText;
	private JTextField cognomeText;
	private JTextField asText;
	Connection conn=null;
	Statement m_Statement=null;
	String query=null;
	ResultSet m_ResultSet=null;
	GUI() throws IOException, SQLException{
		
		conn = DriverManager.getConnection(NAZIONE,USER,PASS);

		    m_Statement = conn.createStatement();
		    query = "SELECT nome FROM nazione";

		    m_ResultSet = m_Statement.executeQuery(query);
		    int i=1;
		    nazione[0] = "Seleziona nazione...";
		    while (m_ResultSet.next()) {
		      nazione[i]=m_ResultSet.getString(1);
		      i++;
		    }
		    //conn.close();
		    //m_Statement.close();
		    //m_ResultSet.close();
		    
		JFrame frame = new JFrame("Scuola");
		frame.setSize(300, 362);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Alunno", null, panel_1, null);
		panel_1.setLayout(null);
		
		nomeText = new JTextField();
		nomeText.setBounds(86, 40, 150, 20);
		panel_1.add(nomeText);
		nomeText.setColumns(10);
		
		cognomeText = new JTextField();
		cognomeText.setColumns(10);
		cognomeText.setBounds(86, 71, 150, 20);
		panel_1.add(cognomeText);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Arial Black", Font.BOLD, 11));
		lblNome.setBounds(9, 43, 46, 14);
		panel_1.add(lblNome);
		
		JLabel lblCognome = new JLabel("Cognome");
		lblCognome.setFont(new Font("Arial Black", Font.BOLD, 11));
		lblCognome.setBounds(9, 74, 67, 14);
		panel_1.add(lblCognome);
		
		JLabel lblDatiAnagrafici = new JLabel("Dati Anagrafici");
		lblDatiAnagrafici.setFont(new Font("Verdana", Font.PLAIN, 11));
		lblDatiAnagrafici.setForeground(Color.RED);
		lblDatiAnagrafici.setBounds(97, 11, 129, 14);
		panel_1.add(lblDatiAnagrafici);
		
		JComboBox regioneBox = new JComboBox(regione);
		regioneBox.setEnabled(false);
		regioneBox.setBounds(141, 151, 95, 20);
		panel_1.add(regioneBox);
		
		JComboBox comuneBox = new JComboBox(comune);
		comuneBox.setEnabled(false);
		comuneBox.setBounds(141, 182, 95, 20);
		panel_1.add(comuneBox);
		
		JLabel lblRegioneNascita = new JLabel("Regione Nascita");
		lblRegioneNascita.setFont(new Font("Arial Black", Font.BOLD, 11));
		lblRegioneNascita.setBounds(9, 150, 122, 21);
		panel_1.add(lblRegioneNascita);
		
		JLabel lblComuneNascita = new JLabel("Comune Nascita");
		lblComuneNascita.setFont(new Font("Arial Black", Font.BOLD, 11));
		lblComuneNascita.setBounds(9, 184, 122, 14);
		panel_1.add(lblComuneNascita);
		
		JButton button = new JButton("Invia dati");
		button.setBounds(97, 223, 89, 23);
		panel_1.add(button);
		
		JLabel lblNazionalit = new JLabel("Nazionalit\u00E0");
		lblNazionalit.setFont(new Font("Arial Black", Font.BOLD, 11));
		lblNazionalit.setBounds(9, 119, 122, 21);
		panel_1.add(lblNazionalit);
		
		JComboBox nazioneBox = new JComboBox(nazione);
		nazioneBox.setSelectedIndex(0);
		
		nazioneBox.setBounds(141, 120, 95, 20);
		panel_1.add(nazioneBox);
		nazioneBox.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				regioneBox.setEnabled(true);
				//nazioneBox.removeItemAt(0);
				
					
					
					
				
				

			
				
					
						
					
				
			    String query = "SELECT citta.nome FROM citta JOIN nazione ON idnaz=fknaz WHERE nazione.nome='"+nazioneBox.getSelectedItem()+"'";
			    System.out.println(nazioneBox.getSelectedItem());
				
					try {
						m_ResultSet = m_Statement.executeQuery(query);
					} catch (SQLException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				
			    int i=0;
			    try {
					while (m_ResultSet.next()) {
					  nazione[i]=m_ResultSet.getString(1);
					  i++;
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Connection conn = null;
				Statement stmt = null;
				try{
				    //STEP 2: Register JDBC driver
				   Class.forName("com.mysql.jdbc.Driver");
				   

				    //STEP 3: Open a connection
				    System.out.println("Connecting to database...");
				    conn = DriverManager.getConnection(SCUOLA,USER,PASS);

				    //STEP 4: Execute a query
				    System.out.println("Creating statement...");
				    stmt = conn.createStatement();
				    String sql;
				    
				    sql = "INSERT INTO datianag (nome,cognome,nazionalita,regione,comune) VALUES ('"+nomeText.getText()+"','"+cognomeText.getText()+"',,'"+nazioneBox.getSelectedItem()+"''"+regioneBox.getSelectedItem()+"','"+comuneBox.getSelectedItem()+"');";
				    stmt.executeUpdate(sql);

				    //STEP 5: Extract data from result set
				    
				    //STEP 6: Clean-up environment
				    stmt.close();
				    conn.close();
				 }catch(SQLException se){
				    //Handle errors for JDBC
				    se.printStackTrace();
				 }catch(Exception e1){
				    //Handle errors for Class.forName
				    e1.printStackTrace();
				 }finally{
				    //finally block used to close resources
				    try{
				       if(stmt!=null)
				          stmt.close();
				    }catch(SQLException se2){
				    }// nothing we can do
				    try{
				       if(conn!=null)
				          conn.close();
				    }catch(SQLException se){
				       se.printStackTrace();
				    }//end finally try
				 }//end try
				 System.out.println("Goodbye!");
			}
		});
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Scuola", null, panel, null);
		panel.setLayout(null);
		
		JComboBox classeBox = new JComboBox(classe);
		classeBox.setBounds(74, 50, 50, 20);
		panel.add(classeBox);
		
		JComboBox sezioneBox = new JComboBox(sezione);
		sezioneBox.setBounds(203, 50, 50, 20);
		panel.add(sezioneBox);
		
		JComboBox indirizzoBox = new JComboBox(indirizzo);
		indirizzoBox.setBounds(100, 81, 153, 20);
		panel.add(indirizzoBox);
		
		JLabel lblClasse = new JLabel("Classe");
		lblClasse.setFont(new Font("Arial Black", Font.BOLD, 11));
		lblClasse.setBounds(10, 53, 54, 14);
		panel.add(lblClasse);
		
		JLabel lblSezione = new JLabel("Sezione");
		lblSezione.setFont(new Font("Arial Black", Font.BOLD, 11));
		lblSezione.setBounds(134, 53, 59, 14);
		panel.add(lblSezione);
		
		JLabel lblIndirizzo = new JLabel("Indirizzo");
		lblIndirizzo.setFont(new Font("Arial Black", Font.BOLD, 11));
		lblIndirizzo.setBounds(10, 84, 80, 14);
		panel.add(lblIndirizzo);
		
		JLabel lblDatiScolastici = new JLabel("Dati Scolastici");
		lblDatiScolastici.setForeground(Color.RED);
		lblDatiScolastici.setFont(new Font("Verdana", Font.PLAIN, 11));
		lblDatiScolastici.setBounds(100, 11, 138, 14);
		panel.add(lblDatiScolastici);
		
		JLabel lblAnnoScolastico = new JLabel("Anno Scolastico");
		lblAnnoScolastico.setFont(new Font("Arial Black", Font.BOLD, 11));
		lblAnnoScolastico.setBackground(Color.WHITE);
		lblAnnoScolastico.setForeground(Color.BLACK);
		lblAnnoScolastico.setBounds(10, 112, 127, 14);
		panel.add(lblAnnoScolastico);
		
		asText = new JTextField();
		asText.setBounds(147, 109, 106, 20);
		panel.add(asText);
		asText.setColumns(10);
		frame.setVisible(true);
		
		JButton btn = new JButton("Invia dati");
		btn.setBounds(85, 159, 89, 23);
		panel.add(btn);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(""));
		lblNewLabel.setBounds(0, 0, 279, 235);
		panel.add(lblNewLabel);
		btn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Connection conn = null;
				Statement stmt = null;
				try{
				    //STEP 2: Register JDBC driver
				   Class.forName("com.mysql.jdbc.Driver");
				   

				    //STEP 3: Open a connection
				    System.out.println("Connecting to database...");
				    conn = DriverManager.getConnection(SCUOLA,USER,PASS);

				    //STEP 4: Execute a query
				    System.out.println("Creating statement...");
				    stmt = conn.createStatement();
				    String sql;
				    
				    sql = "INSERT INTO datiscuola (classe, sezione, anno, indirizzo) VALUES ('"+((String) classeBox.getSelectedItem())+"','"+((String) sezioneBox.getSelectedItem())+"' ,'"+asText.getText()+"' , '"+((String) indirizzoBox.getSelectedItem())+"');";
				    stmt.executeUpdate(sql);

				    //STEP 5: Extract data from result set
				    
				    //STEP 6: Clean-up environment
				    stmt.close();
				    conn.close();
				 }catch(SQLException se){
				    //Handle errors for JDBC
				    se.printStackTrace();
				 }catch(Exception e1){
				    //Handle errors for Class.forName
				    e1.printStackTrace();
				 }finally{
				    //finally block used to close resources
				    try{
				       if(stmt!=null)
				          stmt.close();
				    }catch(SQLException se2){
				    }// nothing we can do
				    try{
				       if(conn!=null)
				          conn.close();
				    }catch(SQLException se){
				       se.printStackTrace();
				    }//end finally try
				 }//end try
				 System.out.println("Goodbye!");
			}
		});
		}

	public static void main(String[] args) throws IOException, SQLException {
		// TODO Auto-generated method stub
		
		GUI g=new GUI();
	
		
	}
}